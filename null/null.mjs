//null is used to empty variable
// Never assign variable by undefined value
// Null is historical bug which is primitive but returns as type:object
let a;
a = 5;

console.log(a);

a = null;

console.log(a);
