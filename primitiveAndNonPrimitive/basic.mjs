//Differences

// "===" works for primitive but not for non-primitive

// console.log(1 === 1);
// console.log("ram" === "ram");

//console.log([1,2]===[1,2])

// ----------------------------------------------------------------------------------------------------

// Primitive type creates a memory location once "let" is defined
//Non-primitive shares a memory location if another type is a copy

// let a = 1;
// let b = a;
// a = 5;

// console.log(a, b);

// let ar1 = [1, 2];
// let ar2 = ar1;

// ar1.push(3);
// console.log(ar1);
// console.log(ar2);
// ----------------------------------------------------------------------------------------------------
//In case of primitive, "===" checks for value
//In case of non-primitive, "===" checks if the memory location is same or not

// let x = 1;
// let y = x;
// let z = 1;

// console.log(x === y);
// console.log(x === z);

// let ar1=[1,2]
// let ar2=ar1
// let ar3=[1,2]

// console.log(ar1===ar2)
// console.log(ar1===ar3)

// let ar1=[1,2]
// let ar2=ar1
// console.log(ar1===ar2) //true because memory location is same
//console.log([1,2]===[1,2]) //false because there isn't any memory location allocated
// ----------------------------------------------------------------------------------------------------

// primitive

// Non-primitive
