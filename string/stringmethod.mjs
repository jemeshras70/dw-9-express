let name="jemesh"
let trim="    jem   ras    "
//method to turn to uppercase
let upperName= name.toUpperCase()

console.log(upperName)
//method to turn to lowercase
let lowerName=upperName.toLowerCase()

console.log(lowerName)

//To know length of string
let nameLength=name.length
console.log(nameLength)

//Trims the space of before & after text
let nameTrim=trim.trim();
console.log(nameTrim)

let nameSt=trim.trimStart();
console.log(nameSt)

let nameEn=trim.trimEnd()
console.log(nameEn)