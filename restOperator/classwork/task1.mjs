let sum = (...a) => {
  let value = a.reduce((pre, cur) => {
    return pre + cur;
  }, 0);

  return value;
};

console.log(sum(1, 2, 3, 5));
