//define object with key name, location, contactNumber
let obj={
    name:"Jem",
    location:"KTM",
    contactNumber:9851456877
}

//get object
console.log(obj)

//change location to gagalphedi
obj.location="gagalphedi"

//get object
console.log(obj)

//delete contactNumber and get object
delete obj.contactNumber
console.log(obj)