//We can't convert all array to object
// We can only convert if we have array like [["name","Jemesh"],["age",25]]
//here we have array of array and inner array has length of 2

let ar = [
  ["name", "Jemesh"],
  ["age", 25],
  ["location", "Sifal"],
];

let info = Object.fromEntries(ar);
console.log(info);
