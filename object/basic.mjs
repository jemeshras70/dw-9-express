// Array is made by the combination of value
// Object if made by the combination of  key value pair
// name:key, "jemesh"=>value
// name:"jemesh" => property

let obj={
    name:"jemesh", //key: value
    age:25,
    isMarried: false
}
//get whole object
console.log(obj)

//get specific object
console.log(obj.name)
console.log(obj.age)
console.log(obj.isMarried)

//change specific element
obj.age=24

console.log(obj)

//delete specific element
delete obj.isMarried
console.log(obj)