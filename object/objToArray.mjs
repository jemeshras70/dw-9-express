let obj = {
  name: "Jemesh",
  age: 25,
  isMarried: false,
};

let keyArr = Object.keys(obj); //Converts keys to array
let valueArr = Object.values(obj); //Converts values to array
let propAtt = Object.entries(obj); //Converts properties to array

console.log(keyArr);
console.log(valueArr);
console.log(propAtt);
