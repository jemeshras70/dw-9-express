let ar1= ["jemesh",29,false]

//ARRAY CAN store multiple value of different data type.

// get whole array
console.log(ar1)

// get specific element of array
console.log(ar1[0])
console.log(ar1[1])
console.log(ar1[2])


//change specific element of array

ar1[1]=30
ar1[2]=true

console.log(ar1[0])
console.log(ar1[1])
console.log(ar1[2])


//delete specific element of array

delete ar1[1]

console.log(ar1)