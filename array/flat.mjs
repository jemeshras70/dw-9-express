let ar1 = [
  1,
  2,
  [1, 3],
  [
    [1, 2],
    [3, 4, 5],
  ],
];

let ar2 = ar1.flat(2); //2 means open 2 layers of array

console.log(ar2);
