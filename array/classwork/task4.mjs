let products = [
  { name: "earphone", price: 1000 },
  { name: "battery", price: 2000 },
  { name: "charger", price: 500 },
];

let prodPrice = products.reduce((prev, cur) => {
  return prev + cur.price;
}, 0);

console.log(prodPrice);

let nameAr = products.map((value, i) => {
  return value.name;
});

let priceAr = products.map((value, i) => {
  return value.price;
});

let priceG700 = products.filter((value, i) => {
  if (value.price > 700) {
    return true;
  } else {
    return false;
  }
});

let desOut = priceG700.map((value, i) => {
  return value.price;
});

let desOutName = priceG700.map((value, i) => {
    return value.name;
  });



console.log(nameAr);
console.log(priceAr);
console.log(desOut);
console.log(desOutName);
