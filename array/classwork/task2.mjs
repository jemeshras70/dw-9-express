// find the product of all element using reduce

let ar = [1, 2, 3, 4, 5];

let value = ar.reduce((pre, curr) => {
  return pre * curr;
}, 1);

console.log(value);
