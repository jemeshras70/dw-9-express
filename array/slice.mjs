let ar=['a','b','c','d','e']

//
// let ar1=ar.slice(1,4) where 1 is the starting index & 4 is the end index (end index must be always +1 of the actual index)
// console.log(ar1)

//if slice method is given only starting index then it will give all remaining array till the end

let ar1=ar.slice(3)
console.log(ar1)
