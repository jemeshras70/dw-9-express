//only return boolean

// let ar1=['a','b','c']

// let ar2=ar1.filter((value,i)=>{
//     if (value ==="a"){
//         return true;
//     }
//     else if (value==="b"){
//         return true;
//     }
//     else{
//         return false;
//     }

// })

// console.log(ar2)


//[1,2,6,3]=>[2,6]

let input=[1,2,6,3]

let output= input.filter((value,i)=>{
    if (value%2===0){
        return true
    }
    else{
        return false
    }
})

console.log(output)

//filter greater than 17

//[1,9,10,18,20,8] => [18,20]

let inp=[1,9,10,18,20,8]

let out= inp.filter((value,i)=>{
    if(value>17){
        return true
    }
    else{
        return false
    }
})

console.log(out)


// filter string["a",1,"b",3] =>["a","b"]

let i=["a",1,"b",3]
let o= i.filter((value,i)=>{
    if(typeof value==='string'){
        return true
    }
    else{
        return false
    }
})

console.log(o)
