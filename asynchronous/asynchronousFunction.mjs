console.log("a");

setTimeout(() => {
  console.log("Timeset Out");
}, 2000);

console.log("b");

//setTimeout & setInterval always executes at last

//setTimeout throws the code to node and after delay of 2s the code is sent to memory queue in node. After JS code is finished node throws the code to JS and it is executed

//Any thing that pushes task to the background(node) is called asynchronous function

//Event Loop:
//It constantly monitors call stack. If the call stack is empty, it takes the function from memory queue to the call stack

