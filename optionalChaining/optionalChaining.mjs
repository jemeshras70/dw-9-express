let info = {
  name: "Jemesh",
  father: {
    age: 60,
  },
};

console.log(info?.father?.age);

// Optional Chaining : ?.

//Result is either undefined or data
