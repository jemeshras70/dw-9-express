// Day1

/*ctrl+b: sidebar toggle
ctrl+~ : Terminal toggle
ctrl+. : Spelling fix(Quick Fix)
ctrl+/: Comment
alt+Shift+a: Multiline Comment
ctrl+c: Stop nodemon

JS
-Synchronous
-Single threaded
-blocking

Data
-number
-string
-boolean

JS is case sensitive language.
+,-,*,/ are called operators.

1+2=3
"jemesh"+"rasaili"="jemeshrasaili"
"jemesh"+" "+"rasaili"="jemesh rasaili"

1+2=3
'1'+'2'='12'
1+'2'='12'
if there is war between string and number, string always wins.
operation are performed between 2 values.

let num1=1;
let num2=10;

let num3=num1+num2; 
let num4=num1-num3;
let num5=num1*num3;
let num6=num1/num3;

console.log(num3);
console.log(num4);
console.log(num5);
console.log(num6);
console.log(num5,num6,num3,num4)

Variable is a container which contains data.

Javascript is a loosely typed language. A variable can store different data types and isn't restricted to a single data type.
JS doesn't need ; (semicolon)
*/

// Day 8

//All method returns something but ppus changes the original array, whereas reverse and sort method returns a value as well as changes the original array

//Number sort doesn't work properly in JS

//[9,10] => Sorting [10,9]
//Number sorting also works same like string sorting

/*
 */

// Git codes
/* send code from  local system to gitlab

git add .
git commit -m "...Message"

git push origin BranchName



get code from gitlab to our local system

git clone link -> It downloads all files/folder

git pull origin main -> It downloads only partial changes 

git checkout -b Branchname ->make new branch (copies everything from copied branch)

git branch -shows available branches

git checkout BranchName -branch switching

*/

//Git

// in order to work on git in the folder we need to write: git init
