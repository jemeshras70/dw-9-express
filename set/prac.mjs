//[1,2,1,["a","b"],["a","b"]]====[1,2,["a,"b]]
//Solution 1
/* let input = [1, 2, 1, ["a", "b"], ["a", "b"]];

let list = input.map((value, index) => {
  return JSON.stringify(value);
});

let set1 = [...new Set(list)];
console.log(set1);

let output = set1.map((value, index) => {
  return JSON.parse(value);
});

console.log(output); */

//ALTERNATIVE using function

let output = (input) => {
  let list = input.map((value, index) => {
    return JSON.stringify(value);
  });

  let set1 = [...new Set(list)];
  console.log(set1);

  let output = set1.map((value, index) => {
    return JSON.parse(value);
  });

  return output;
};

console.log(output([1, 2, 1, ["a", "b"], ["a", "b"], 4, 5, 6, 6, 5, 7, 2]));
