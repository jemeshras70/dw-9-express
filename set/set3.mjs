let unique = new Set([
  1,
  2,
  3,
  1,
  { name: "Jem" },
  { name: "Jem" },
  null,
  null,
  undefined,
  undefined,
]);

console.log(unique);

//Set only removes duplicate value of primitive type where "null" is an exception
