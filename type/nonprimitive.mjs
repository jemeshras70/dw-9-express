let b = [1, 2, 3];

console.log(typeof b);

let c = new Set([1, 2, 3, 1]);

console.log(typeof c);

let e = new Error();

console.log(typeof e);

// It means type of non-primitive is object
